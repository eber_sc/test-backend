<?php


class tratarDados
{
    function corrigirDados($dados){
        $remove_month = false;

        foreach ($dados as $item => $key){
            ksort($dados[$item]);

            $arrAux = $dados[$item];

            foreach ($arrAux as $prod => $value){

                $arrAux[$prod] = $this->corrige_nome_prod($arrAux[$prod]);

                arsort($arrAux[$prod]);
                count($arrAux[$prod]) == 0 ? $remove_month = true : false;
            }

            $dados[$item] = $arrAux;

            if($remove_month){
                unset($dados[$item]);
            }

        }
        return $dados;
    }

    function corrige_nome_prod($arrDados = array()){

        $array_buscar = array('Papel Hignico', 'Brocolis', 'Chocolate ao leit', 'Sabao em po');
        $array_substituir = array('Papel Higiênico', 'Brócolis', 'Chocolate ao leite', 'Sabão em pó');

        foreach ($arrDados as $item => $value){
            $pos = array_search($item, $array_buscar);

            if($pos !== FALSE) {
                $arrDados[$array_substituir[$pos]] = $arrDados[$item];
                unset($arrDados[$item]);
            }

        }

        return $arrDados;
    }

}
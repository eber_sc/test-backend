<?php

class gerarCSV
{
    function array2csv($dados = array())
    {
        $arrColunas = array('Mês', 'Categoria', 'Produto', 'Quantidade');
        $dados_formatados = $this->convert_multi_array($dados);

        $output = fopen('compras-do-ano.csv', 'w');
        fputcsv($output, $arrColunas);
        foreach ($dados_formatados as $linha) {
            fputcsv($output, $linha);
        }
        fclose($output);
    }

    function convert_multi_array($array) {
        $lista = [];

        foreach ($array as $mes => $categorias) {
            foreach ($categorias as $produtos => $quantidade) {
                foreach ($quantidade as $item => $key) {
                    $lista[] = array($mes, $produtos, $item, $key);
                }
            }
        }
        return $lista;
    }
}
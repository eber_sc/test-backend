<?php

require_once ('GerarCSV.php');
require_once('TratarDados.php');
$dados = require_once('lista-de-compras.php');

$gerarCSV = new gerarCSV();
$tratarDados = new tratarDados();

uksort( $dados, "ordenarMeses");

function ordenarMeses ($a, $b ) {

    $meses = array( 'janeiro', 'fevereiro', 'marco', 'abril', 'maio', 'junho', 'julho', 'agosto', 'setembro', 'outubro', 'novembro', 'dezembro' );
    if ( array_search( $a, $meses) == array_search( $b, $meses) ) return 0;
    return array_search( $a, $meses) > array_search( $b, $meses) ? 1 : -1;
}

$dados = $tratarDados->corrigirDados($dados);

$gerarCSV->array2csv($dados);



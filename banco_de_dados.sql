CREATE DATABASE compras_ano;

USE compras_ano;

CREATE TABLE meses (
    id INT PRIMARY KEY AUTO_INCREMENT,
    nome VARCHAR(15)
);

CREATE TABLE categorias (
    id INT PRIMARY KEY AUTO_INCREMENT,
    nome_categoria VARCHAR(20)
);

CREATE TABLE produtos (
    id INT PRIMARY KEY AUTO_INCREMENT,
    nome_produto VARCHAR(30)
);
      
CREATE TABLE lista_compras (
    id INT PRIMARY KEY AUTO_INCREMENT,
    id_mes INT NOT NULL,
    id_categoria INT NOT NULL,
    id_produto INT NOT NULL,
    quantidade INT
);

ALTER TABLE lista_compras ADD CONSTRAINT fk_mes FOREIGN KEY (id_mes) REFERENCES meses (id);
ALTER TABLE lista_compras ADD CONSTRAINT fk_categoria FOREIGN KEY (id_categoria) REFERENCES categorias (id);
ALTER TABLE lista_compras ADD CONSTRAINT fk_produto FOREIGN KEY (id_produto) REFERENCES produtos (id);

INSERT INTO meses (nome)
VALUES('janeiro'),('fevereiro'),('marco'),('abril'),('maio'),('junho'),('julho'),('agosto'),('setembro'),('outubro'),('novembro'),('dezembro');

INSERT INTO categorias (nome_categoria)
VALUES ('alimentos'), ('higiene_pessoal'),('limpeza');

INSERT INTO produtos(nome_produto)
VALUES ('Arroz'), ('Arroz integral'), ('Beringela'), ('Arroz'),
('Brócolis'), ('Chocolate ao leite'), ('Creme dental'), ('Desinfetante'), ('Detergente'), ('Diabo verde'), ('Doritos'),
('Enxaguante bocal'), ('Escova de dente'), ('Esponja de aço'),
('Feijão'), ('Filé de frango'),('Filé Mignon'),('Fio dental'),('Geléria de morango'),('Iogurte'),('Leite desnatado'),('MOP'),('Morango'),
('Nutella'),('Ovos'),('Pano de chão'),('Pão de forma'),('Papel Higiênico'),('Pasta de Amendoim'),('Pepino'),('Protex'),('Queijo minas'),('Rodo'),('Sabão em pó'),('Sabonete Dove'),('Sabonete Protex'),('Shampoo'),('Tomate'),('Veja multiuso'); 
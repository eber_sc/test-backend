<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once ('TratarDados.php');
$dados = require_once ('lista-de-compras.php');

$tratarDados = new tratarDados();

$dados = $tratarDados->corrigirDados($dados);

$username = 'root';
$password = '';
try{
    
    cadastra_dados($dados);

} catch (Exception $ex){
    echo 'Erro: ' . $ex->getMessage();
}

function executar_query($query){
	try{
		$conexao = mysqli_connect('localhost', 'root', '', 'compras_ano');
		$res = mysqli_query($conexao, $query);
		mysqli_close($conexao);
		return $res;
	} catch (Exception $ex){
		$ex->getMessage();
	}
}


//debug($dados);

function debug($var){
    echo '<pre>';
    print_r($var);
    die();
}

function cadastra_dados($dados){
    $dados = convert_multi_array($dados);

    $arrDados = array();

    try {
        foreach ($dados as $value => $key) {
            $arrDados['id_mes'] = getMesId($key[0]);
            $arrDados['id_categoria'] = getCategoriaId($key[1]);
            $arrDados['id_produto'] = getProdutoId($key[2]);
            $arrDados['quantidade'] = $key[3];
            $colunas = array_keys($arrDados);
            $valores = implode(",", array_values($arrDados));
            $lista_colunas = implode(",", $colunas);
			
            $sql = "INSERT INTO lista_compras($lista_colunas) values($valores)";
			executar_query($sql);
        }
    }catch (Exception $ex){
        echo $ex->getMessage();
    }
	echo 'Dados inseridos com sucesso!';
}

function getMesId($mes){

    $resul = executar_query("SELECT id from meses where nome like '%".$mes."%'");
	while($linha=mysqli_fetch_array($resul)){
		return $linha["id"];
	}
    return "";
}

function getCategoriaId($categoria){
	$resul = executar_query("SELECT id from categorias where nome_categoria like '%".$categoria."%'");
	while($linha=mysqli_fetch_array($resul)){
		return $linha["id"];
	}
    return "";
}

function getProdutoId($produto){
	$resul = executar_query("SELECT id from produtos where nome_produto like '%".$produto."%'");
	while($linha=mysqli_fetch_array($resul)){
		return $linha["id"];
	}
    return "";
}

function convert_multi_array($array) {
    $lista = [];

    foreach ($array as $mes => $categorias) {
        foreach ($categorias as $produtos => $quantidade) {
            foreach ($quantidade as $item => $key) {
                $lista[] = array($mes, $produtos, $item, $key);
            }
        }
    }
    return $lista;
}